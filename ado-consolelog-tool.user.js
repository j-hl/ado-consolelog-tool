// ==UserScript==
// @name ADO ConsoleLog Tool
// @namespace https://bitbucket.org/j-hl/ado-consolelog-tool
// @description Useful information about ADO campaigns. Just open de console to see ADO logs.
// @author J-HL
// @version 1.0
// @encoding utf-8
// @license https://creativecommons.org/licenses/by-nc-sa/4.0/
// @icon 
// @homepage https://bitbucket.org/j-hl/ado-consolelog-tool/
// @supportURL https://bitbucket.org/j-hl/ado-consolelog-tool/issues?status=new&status=open
// @updateURL https://bitbucket.org/j-hl/ado-consolelog-tool/raw/99a4ffdce530d473fa29584037769c7f39a79a98/ado-consolelog-tool.user.js 
// @downloadURL https://bitbucket.org/j-hl/ado-consolelog-tool/
// @require http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js
// @match *://*/*
// @run-at document-end
// ==/UserScript==
// 


function exec(fn) {
    var script = document.createElement('script');
    script.setAttribute("type", "application/javascript");
    script.textContent = '(' + fn + ')();';
    document.body.appendChild(script); // run the script
    //document.body.removeChild(script); // clean up
}

exec(function () {

    //ADO TOOL (unofficial)
    $(document).ready(function () {
        function colorlog(type, msg, color) {
            if (type == "log") {
                console.log("%c" + msg, "color:" + color + ";font-weight:bold;");
            } else {
                console.group("%c" + msg, "color:" + color + ";font-weight:bold;");
            }
        }
        try {
            if (window.ADO.visitorInfo.visitorKey !== undefined) {
                console.log(":::::::::::::::::::::::::::::::::::::::::::::::::::::::::");
                colorlog("group", "ADO CONSOLELOG TOOL", "black");
                console.log("Account Name: " + window.ADO.accountName);
              for (var i = 0; i < Object.keys(window.ADO.treatments).length; i++) {
                  var campaign = Object.keys(window.ADO.treatments)[i];
                console.group("Campaign " + (i+1) + ": " + campaign);
                console.log("Allocated Version: " + Object.values(window.ADO.treatments[campaign])[1]);
                console.groupEnd()
              }
                console.groupEnd()
              
            }
        } catch (e) {
            if (e) {}
        }
    });

}); 
